import aiohttp
import asyncio

from client import (
    Client,
)
from client.utils import (
    get_register_data,
    get_auth_data,
    show_register_message,
    show_auth_message,
    show_get_user_message,
    get_update_data,
    show_update_message,
)


async def main():
    async with aiohttp.ClientSession() as session:
        client = Client(
            host="http://0.0.0.0",
            port=8080,
            session=session,
        )
        register_data = await get_register_data()
        auth_data = await get_auth_data()

        try:
            # Регистрация пользователя.
            await show_register_message(
                data=register_data,
                client=client,
            )

            # Авторизация пользователя.
            auth_response = await show_auth_message(
                data=auth_data,
                client=client,
            )
            token = auth_response.get(
                "token",
                "",
            )

            # Отображение пользователя.
            get_user_data = {
                "username": "admin",
                "token": token,
            }
            await show_get_user_message(
                data=get_user_data,
                client=client,
            )

            # Обновление пользователя.
            update_data = await get_update_data(token=token)
            await show_update_message(
                data=update_data,
                client=client,
            )

            # Отображение обновленного пользователя.
            await show_get_user_message(
                data=get_user_data,
                client=client,
            )

        except aiohttp.ClientConnectorError:
            print("Произошла ошибка.")


loop = asyncio.get_event_loop()
loop.run_until_complete(main())
