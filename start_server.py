from aiohttp import web

from server.views import auth, register, get_user, update

app = web.Application()
app.add_routes(
    [
        web.post("/auth", auth),
        web.post("/register", register),
        web.get("/get_user/{username}/", get_user),
        web.post("/user/update/{username}/", update),
    ]
)

if __name__ == "__main__":
    web.run_app(app)
