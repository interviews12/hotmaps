import uuid
from datetime import datetime

from .db import user_data, db_id


class ClientDB:
    async def update_user_data(self, login: str, token: str, data: dict) -> str:
        if user_data.get(login) and (token in user_data.get(login).get("tokens", [])):

            if await self.validate_data(data):
                user_data.get(login).update(data)
                return "Ok"

            else:
                return "Error"

        else:
            return "Not found"

    async def get_new_db_id(self) -> int:
        value = db_id["value"]
        db_id["value"] = value + 1

        return value

    async def create_new_user(self, login: str, password: str, name: str) -> bool:
        """Create new user and return bool status."""

        if user_data.get(login):
            return False

        user_data[login] = {
            "id": await self.get_new_db_id(),
            "password": password,
            "tokens": [],
            "active": "1",
            "blocked": False,
            "created_at": int(datetime.timestamp(datetime.now())),
            "name": str(name),
            "permissions": [],
        }

        return True

    async def get_user_data(self, login: str, token: str) -> dict:
        """Return user data."""

        if user_data.get(login) and (token in user_data.get(login).get("tokens", [])):
            result = user_data.get(login).copy()

            del result["password"]
            del result["tokens"]

            return result

        else:
            return {}

    async def get_new_user_token(self, login: str, password: str) -> str:
        """Return created token for user."""

        if user_data.get(login) and (user_data.get(login)["password"] == password):
            token = str(uuid.uuid4())[:24]
            user_data.get(login)["tokens"].append(token)

            return token

        return ""

    async def validate_data(self, data: dict) -> bool:
        """Return validate bool status."""

        if not isinstance(data, dict):
            return False

        if ["tokens", "created_at", "id"] in list(data.keys()):
            return False

        if len(data.keys()) > 10:
            return False

        for key in data.keys():
            if not (
                key
                in [
                    "password",
                    "active",
                    "blocked",
                    "name",
                    "permissions",
                ]
            ):
                return False

        if not isinstance(data.get("password", ""), str):
            return False

        if not isinstance(data.get("active", 1), int):
            return False

        if not isinstance(data.get("blocked", True), bool):
            return False

        if not isinstance(data.get("name", ""), str):
            return False

        if not isinstance(data.get("permissions", []), list):
            return False

        return True


client_db = ClientDB()
