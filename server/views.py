from aiohttp import web

from .client_db import client_db


async def auth(request):
    """Return new token for user."""

    data = await request.json()

    # Проверяем тип данных.
    if not isinstance(data, dict):
        return web.json_response({"status": "Error"})

    login = data.get("login", "")
    password = str(data.get("password", ""))

    # Получаем токен.
    if login and password:
        token = await client_db.get_new_user_token(login=login, password=password)

        if token:
            return web.json_response({"status": "Ok", "token": token})

    return web.json_response({"status": "Not found"})


async def get_user(request):
    """Return user info."""

    username = request.match_info.get("username", "")
    token = request.query.get("token", "")

    if username and token:
        user_data = await client_db.get_user_data(username, token)

        if user_data:
            user_data["status"] = "Ok"
            return web.json_response(user_data)

    return web.json_response({"status": "Not found"})


async def update(request):
    """Return status of user updating."""

    username = request.match_info.get("username")
    token = request.query.get("token")

    data = await request.json()
    if not isinstance(data, dict):
        return web.json_response({"status": "Error"})

    if username and token and data:
        status = await client_db.update_user_data(login=username, token=token, data=data)
        return web.json_response({"status": status})

    else:
        return web.json_response({"status": "Not found"})


async def register(request):
    """Create new user."""

    data = await request.json()

    if isinstance(data, dict):
        if await client_db.create_new_user(**data):
            return web.json_response({"status": "Ok"})

    return web.json_response({"status": "Error"})
