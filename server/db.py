db_id = {"value": 1}

user_data = {
    "test": {
        "id": 23,
        "password": "12345",
        "tokens": ["dsfd79843r32d1d3dx23d32d"],
        "active": "1",
        "blocked": False,
        "created_at": 1587457590,
        "name": "Ivanov Ivan",
        "permissions": [
            {"id": 1, "permission": "comment"},
            {"id": 2, "permission": "upload photo"},
            {"id": 3, "permission": "add event"},
        ],
    }
}
