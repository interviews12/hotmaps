import json

from aiohttp.test_utils import AioHTTPTestCase, unittest_run_loop
from aiohttp import web

from server.views import register, update, get_user, auth


class MyAppTestCase(AioHTTPTestCase):
    async def get_application(self):

        app = web.Application()

        app.router.add_post("/auth", auth)
        app.router.add_post("/register", register)
        app.router.add_get("/get_user/{username}/", get_user)
        app.router.add_post("/user/update/{username}/", update)

        return app

    @unittest_run_loop
    async def test_register_success(self):
        resp = await self.client.post(
            "/register",
            data=json.dumps(
                {
                    "login": "admin",
                    "password": "pass",
                    "name": "Vladislav",
                }
            ),
        )
        assert resp.status == 200
        response = await resp.json()
        assert {"status": "Ok"} == response

    @unittest_run_loop
    async def test_register_error(self):
        resp = await self.client.post(
            "/register",
            data=json.dumps(
                {
                    "login": "test",
                    "password": "pass",
                    "name": "Vladislav",
                }
            ),
        )
        assert resp.status == 200
        response = await resp.json()
        assert {"status": "Error"} == response

    @unittest_run_loop
    async def test_auth_success(self):
        resp = await self.client.post(
            "/auth",
            data=json.dumps({"login": "test", "password": "12345"}),
        )
        assert resp.status == 200
        response = await resp.json()
        assert response.get("status") == "Ok"

    @unittest_run_loop
    async def test_auth_error(self):
        resp = await self.client.post(
            "/auth",
            data=json.dumps({"login": "test", "password": "123456"}),
        )
        assert resp.status == 200
        response = await resp.json()
        assert response.get("status") == "Not found"

    @unittest_run_loop
    async def test_get_user_success(self):
        resp = await self.client.get("/get_user/test/?token=dsfd79843r32d1d3dx23d32d")
        assert resp.status == 200
        response = await resp.json()
        assert response == {
            "id": 23,
            "active": "1",
            "blocked": False,
            "created_at": 1587457590,
            "name": "Ivanov Ivan",
            "permissions": [
                {"id": 1, "permission": "comment"},
                {"id": 2, "permission": "upload photo"},
                {"id": 3, "permission": "add event"},
            ],
            "status": "Ok",
        }

    @unittest_run_loop
    async def test_get_user_error(self):
        resp = await self.client.get("/get_user/not_found/?token=1124")
        assert resp.status == 200
        response = await resp.json()
        assert response.get("status") == "Not found"

    @unittest_run_loop
    async def test_update_success(self):
        resp = await self.client.post(
            "/user/update/test/?token=dsfd79843r32d1d3dx23d32d",
            data=json.dumps({"name": "Vladislav Kashirin"}),
        )
        assert resp.status == 200
        response = await resp.json()
        assert response.get("status") == "Ok"

    @unittest_run_loop
    async def test_update_error(self):
        resp = await self.client.post(
            "/user/update/test/?token=dsfd79843r32d1d3dx23d32d",
            data=json.dumps({"names": "Vladislav Kashirin"}),
        )
        assert resp.status == 200
        response = await resp.json()
        assert response.get("status") == "Error"
