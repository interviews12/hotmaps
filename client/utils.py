from client import (
    Client,
)


async def get_register_data():
    return {
        "login": "admin",
        "password": "pass",
        "name": "Vladislav",
    }


async def get_auth_data():
    return {
        "login": "admin",
        "password": "pass",
    }


async def get_update_data(
    token: str,
) -> dict:
    return {
        "username": "admin",
        "data": {"name": "Vladislav Kashirin"},
        "token": token,
    }


async def show_register_message(
    data: dict,
    client: Client,
) -> dict:
    """Show register new user messages."""

    print("Тестовое представление клиента :-)")
    print(f"\nРегистрирую пользователя: {data}")

    response = await client.register(data)
    print(
        "Ответ от сервера:",
        response,
    )

    return response


async def show_auth_message(
    data: dict,
    client: Client,
) -> dict:
    """Show auth user messages."""

    print(f"\nАвторизуюсь: {data}")
    auth_response = await client.auth(data)

    print(
        f"Ответ от сервера:",
        auth_response,
    )
    return auth_response


async def show_get_user_message(
    data: dict,
    client: Client,
):
    """Show user data messages."""

    print(
        "\nПолучаю данные пользователя:",
        data,
    )
    response = await client.get_user(data)
    print(
        "Ответ от сервера:",
        response,
    )


async def show_update_message(
    data: dict,
    client: Client,
):
    """Show user updating messages."""

    print(
        "\nОбновляю данные пользователя:",
        data,
    )
    print(
        "Ответ от сервера:",
        await client.update(data),
    )
