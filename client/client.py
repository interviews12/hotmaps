import json

import aiohttp


class Client:
    def __init__(
        self,
        host: str,
        port: int,
        session: aiohttp.ClientSession,
    ):
        self.base_host = host
        self.port = port
        self.session = session

        self.host = f"{self.base_host}:{self.port}"

    async def _post_request(self, host: str, data: dict) -> dict:
        """Return post requests response."""

        async with self.session.post(host, data=json.dumps(data)) as response:
            return await response.json()

    async def _get_request(
        self,
        host: str,
    ) -> dict:
        """Return get requests response."""

        async with self.session.get(host) as response:
            return await response.json()

    async def register(
        self,
        data: dict,
    ) -> dict:
        """Register new user."""
        return await self._post_request(
            f"{self.host}/register",
            data=data,
        )

    async def auth(
        self,
        data: dict,
    ) -> dict:
        """Return user token."""
        return await self._post_request(
            f"{self.host}/auth",
            data=data,
        )

    async def get_user(
        self,
        data: dict,
    ) -> dict:
        """Return user data."""
        return await self._get_request(
            f"{self.host}/get_user/{data.get('username')}/?token={data.get('token')}"
        )

    async def update(
        self,
        data: dict,
    ) -> dict:
        """Update user data."""
        return await self._post_request(
            f"{self.host}/user/update/{data.get('username')}/?token={data.get('token')}",
            data=data.get("data"),
        )
